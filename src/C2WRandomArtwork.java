import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.util.Random;

public class C2WRandomArtwork extends Application{
    
    private static final int width = 750;
    private static final int height = 600;
    private static final int timeInterval = 2000; // 2 seconds

    private Canvas canvas;
    private Thread animationThread;
    private boolean running;

    @Override
    public void start(Stage primaryStage) {
        canvas = new Canvas(width, height);
        Button startButton = new Button("Start");
        //ActionEvent e=new ActionEvent(primaryStage, startButton);
        //EventHandler<ActionEvent> eh=new 

        startButton.setOnAction(e -> startAnimation());

        VBox root = new VBox(canvas, startButton);
        primaryStage.setScene(new Scene(root));
        primaryStage.setTitle("Core2Web Random Art Generator");
        primaryStage.show();
    }

    private void startAnimation() {
        if (animationThread != null && animationThread.isAlive()) {
            // Animation is already running, no need to start again
            return;
        }

        running = true;
        animationThread = new Thread(() -> {
            while (running) {
                generateRandomArtwork();
                updateCanvas();

                try {
                    Thread.sleep(timeInterval);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        animationThread.start();
    }

    private void generateRandomArtwork() {
        Random random = new Random();
        GraphicsContext gc = canvas.getGraphicsContext2D();

        // Clear canvas
        gc.setFill(Color.WHITE);
        gc.fillRect(0, 0, width, height);

        // Generate random shapes
        for (int i = 0; i < 10; i++) {
            int x = random.nextInt(width);
            int y = random.nextInt(height);
            int width = random.nextInt(100);
            int height = random.nextInt(100);

            Color color = Color.rgb(
                    random.nextInt(256),
                    random.nextInt(256),
                    random.nextInt(256)
            );

            gc.setFill(color);
            gc.fillRect(x, y, width, height);
        }
    }

    private void updateCanvas() {
        Platform.runLater(() -> {
            // Redraw the canvas on the JavaFX application thread
            canvas.getGraphicsContext2D().drawImage(canvas.snapshot(null, null), 0, 0);
        });
    }

    @Override
    public void stop() {
        running = false;
        animationThread.interrupt();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
